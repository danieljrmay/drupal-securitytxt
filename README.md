<!-- markdownlint-disable MD033 -->
# <img src="logo.png" width="100" alt="logo"/> Drupal Security.txt

[![pipeline status](https://gitlab.com/danieljrmay/drupal-securitytxt/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/drupal-securitytxt/commits/master)
[![Copr build status](https://copr.fedorainfracloud.org/coprs/danieljrmay/drupal-packages/package/drupal7-securitytxt/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/danieljrmay/drupal-packages/package/drupal7-securitytxt/)

RPM packaging for the
[Security.txt](https://www.drupal.org/project/securitytxt) Drupal 7
module. No RPM is currently available for the Drupal 8/9 version of
the module as most people as moving away from using RPMs for their
drupal installations.

## Copr Respository

If you are using Fedora, RHEL or CentOS (or similar) then you can
install the `drupal7-securitytxt` RPM via a [Copr
repository](https://copr.fedorainfracloud.org/coprs/danieljrmay/drupal-packages/).

```console
dnf copr enable danieljrmay/drupal-packages
dnf install drupal7-securitytxt
```
