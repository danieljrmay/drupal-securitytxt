%{?drupal7_find_provides_and_requires}

%global module_name securitytxt

Name:          drupal7-%{module_name}
Version:       1.0
Release:       1%{?dist}
Summary:       Security.txt module for Drupal

License:       GPLv2+
URL:           https://www.drupal.org/project/%{module_name}
Source0:       https://ftp.drupal.org/files/projects/%{module_name}-7.x-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: drupal7-rpmbuild >= 7.23-3

Requires:      drupal7


%description
Provides the Security.txt module for Drupal 7.


%prep
%setup -q -n %{module_name}


%build
# Empty build section, nothing to build


%install
mkdir -p -m 0755 %{buildroot}%{drupal7_modules}/%{module_name}
cp -pr * %{buildroot}%{drupal7_modules}/%{module_name}/


%files
%{drupal7_modules}/%{module_name}
%exclude %{drupal7_modules}/%{module_name}/*.org
%doc README.txt

%changelog
* Tue Nov  6 2018 Daniel J. R. May <daniel.may@kada-media.com> - 1.0-1
- Initial release.
